package main

import (
	"fmt"
	"io"
	"log"
	"net/http"
)

func main() {

	// Define the route where the webhook will receive requests
	http.HandleFunc("/webhook", webhookHandler)

	// Start the server
	fmt.Println("Webhook server is running on port 3000...")
	log.Fatal(http.ListenAndServe(":3000", nil))

}

func webhookHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodPost {
		http.Error(w, "method not allowed", http.StatusMethodNotAllowed)
		return
	}

	body, err := io.ReadAll(r.Body)

	if err != nil {
		http.Error(w, "error reading request body", http.StatusBadRequest)
		return
	}

	fmt.Println("recieved webhook payload..!")
	fmt.Printf("response body: %s \n", string(body))

	w.WriteHeader(http.StatusOK)
	w.Write([]byte("Webhook recieved successfully"))
}
