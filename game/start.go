package main

import (
	"fmt"
	"log"
)

type PlayerData struct {
	health   int
	level    int
	name     string
	location string
}

type Ammunition struct {
	grenades   int
	smokeBombs int
}

type PlayerScore struct {
	coins  int
	points int
	medal  string
}

var Player *PlayerData
var Ammo *Ammunition
var Score *PlayerScore

func init() {
	log.Println("Game has been Started..!")
	log.Println("Initializing the player & ammuniation data")
	Player = &PlayerData{
		health:   100,
		level:    1,
		name:     "Tinesh Katta",
		location: "India",
	}

	Ammo = &Ammunition{
		grenades:   15,
		smokeBombs: 10,
	}

	Score = &PlayerScore{
		coins:  1000,
		points: 50,
		medal:  "bronze",
	}
	log.Printf("%s from %s is now playing..!", Player.name, Player.location)
}

func (player *PlayerData) TakeDamageFromBlast(blastDamage int) {

	if player.health >= 0 && blastDamage <= 75 {
		player.health = player.health - blastDamage
	}
}

func (ammo *Ammunition) ThrowGrenades(grenades int) int {
	var totalDamage int
	if grenades >= 0 && grenades <= ammo.grenades {
		ammo.grenades = ammo.grenades - grenades
		totalDamage = 10 * grenades
		log.Printf("Damage taken by the player:%d", totalDamage)
	} else {
		log.Fatalf("max number of grenades present is %d", Ammo.grenades)
	}
	return totalDamage
}

func (s *PlayerScore) UpdateScore(blastDamage int) {
	if blastDamage > 75 {
		s.coins = s.coins - blastDamage*3
		s.points = s.coins - 10
		printScoreInfo()
		log.Fatalf("Game Over due to high health damaage by bomb blast: %d", blastDamage)
	} else {
		s.coins = s.coins + blastDamage*2
		s.points = s.coins + 5
		printScoreInfo()
	}

}

func printScoreInfo() {
	log.Println("Coins Updated:", Score.coins)
	log.Println("Points Update:", Score.points)
}

func main() {
	var grenades int
	fmt.Println("enter no of grenades to throw:")
	fmt.Scanf("%d", &grenades)
	damage := Ammo.ThrowGrenades(grenades)
	fmt.Println("No of Grenades remaining:", Ammo.grenades)
	Player.TakeDamageFromBlast(damage)
	log.Println("Player Health after Damage:", Player.health)
	Score.UpdateScore(damage)

}
